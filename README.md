Requirements:
  - A Raspberry Pi with Ethernet (USB is okay).
    The Raspberry Pi Zero should be good for 50mbps internet.
    A low memory Pi 4 should be good for Gigabit operation.
  - A preferably rooted android phone with USB Tethering set as the default 
    USB Mode. You can do this in Developer Settings, at least on Pie.
    With root you can route your tether traffic through a local VPN with
    [VPN hotspot](https://play.google.com/store/apps/details?id=be.mygod.vpnhotspot&hl=en_US).
    This usually yields faster unlimited internet.
  - For better range, use a real Wi-Fi router... This doesn't support using
    the Raspberry Pi antennas, feel free to PR!

Steps:

1. Router must be connected before Raspberry pi startup

Todo:
  - Document how to monitor log for this.
  - Make a node monitor to better manage spotty system events
  - Expose gateway IP on dnsmasq
